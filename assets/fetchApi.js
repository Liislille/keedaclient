

async function fetchRecipes() {
    let result = await fetch(`${API_URL}/recipe`);
    let recipes= await result.json();
    return recipes;   
}

async function fetchRandomRecipe(mealCategory, timeCategory) {
    let result = await fetch(`${API_URL}/recipe/${mealCategory}/${timeCategory}`);
    let recipe = await result.json();
    return recipe;  
}


async function saveRecipe(recipe) {
    await fetch(`${API_URL}/recipe/edit`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(recipe)
    });
    
}

async function fetchMealCatTypes() {
    let response = await fetch(`${API_URL}/recipe/category`);
    return  response.json();
}

