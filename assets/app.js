// Initialization after the HTML document has been loaded...
window.addEventListener('load', () => {
    document.getElementById('generateRandomRecipe').addEventListener('click', doLoadRandomRecipe);
    // Function calls for initial page loading activities...
    doLoadCategories();
});



async function doLoadCategories() {
    let categories = await fetchMealCatTypes();
    console.log(categories)
}

async function doLoadRandomRecipe() {
    let mcelements = document.querySelectorAll("[name=mealCategory]");
    let selectedMealCategory = null;

    for (i = 0; i < mcelements.length; i++) {
        if (mcelements[i].checked) {
            selectedMealCategory = mcelements[i].value;
        }
    }

    let mtelements = document.querySelectorAll("[name=timeCategory]");
    let selectedTimeCategory = null;

    for (i = 0; i < mtelements.length; i++) {
        if (mtelements[i].checked) {
            selectedTimeCategory = mtelements[i].value;
        }
    }

    let recipe = await fetchRandomRecipe(selectedMealCategory, selectedTimeCategory);
    var wrapper = document.getElementById("recepieWrapper");
    wrapper.style.display = "block";

    var header = document.getElementById("recepieHeader");
    var body = document.getElementById("recepieBody")
    header.innerHTML = recipe.mealName
    body.innerHTML = recipe.recipe
    scrollFunction();
}
function scrollFunction() {
    var elmnt = document.getElementById("content");
    elmnt.scrollIntoView({ block: 'end', behavior: 'smooth' });
}


async function doOpenPopup(recipeId) {
    await openPopup(POPUP_CONF_DEFAULT, 'recipeAddTemplate');

    if (recipeId > 0) {
        console.log('Muuda retsepti');
        let recipe = await fetchRecipes(recipeId);
        console.log(recipe);
        let mealNameText = document.querySelector('#mealName');
        let mealCategoryText = document.querySelector('#mealCategory');
        let timeCategoryText = document.querySelector('#timeCategory');
        let recipeText = document.querySelector('#recipe');

        mealNameText.value = recipe.mealName;
        mealCategoryText.value = recipe.mealCategory;
        timeCategoryText = recipe.timeCategory;
        recipeText = recipe.recipe
        currentRecipeId = recipe.id;
    } else {
        console.log('Retsepti lisamine');
        currentRecipeId = 0;
    }
}

async function doSaveRecipe() {
    console.log('Retsepti salvestamine');
    let mealNameText = document.querySelector('#mealName');
    let mealCategoryText = document.querySelector('#mealCategory');
    let timeCategoryText = document.querySelector('#timeCategory');
    let recipeTextBox = document.querySelector('#recipe');
    let alertBox = document.getElementById('success-alert')
    alertBox.style.display = "block";
    setTimeout(function () {
        let alertBox = document.getElementById('success-alert')
        alertBox.style.display = "none";
    }, 3000);

    let recipeName = mealNameText.value;
    let recipeCategory = mealCategoryText.value;
    let recipeTime = timeCategoryText.value;
    let recipeText = recipeTextBox.value;



    let recipe = {
        id: currentRecipeId,
        mealName: recipeName,
        timeCategory: recipeTime,
        mealCategory: recipeCategory,
        recipe: recipeText

    };
    console.log(recipe);

    saveRecipe(recipe);
    await doLoadRecipes();
    await closePopup();

}


