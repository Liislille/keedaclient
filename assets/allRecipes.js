// Initialization after the HTML document has been loaded...
window.addEventListener('load', () => {
    doLoadRecipes();
});

async function doLoadRecipes() {
    let recipes = await fetchRecipes();
    let ul = document.getElementById('otsing')

    for(i=0; i < recipes.length; i++){
        let recipe = recipes[i]
        var entry = document.createElement('li');
        entry.appendChild(document.createTextNode(recipe.mealName));
        ul.appendChild(entry);
        console.log(recipe)
    }
}